=================================
aioagi version 1.1.1 release notes
=================================

Welcome to the aioagi 1.1.1 release.
Async server for work with AGI protocol of telephony server Asterisk.

Using Python3.6+.

Changes and new features
========================
The features introduced include:

* Bug fix in ami module.