import asyncio
import logging.config

from aioagi.log import agi_client_logger
from aioagi.client import AGIClientSession
from aioagi.parser import AGIMessage, AGICode


async def test_request(loop):
    headers = {
        # Session request headers are set automatically.
        # 'agi_type': 'SIP',
        # 'agi_network': 'yes',
        # 'agi_network_script': 'agi/',
        # 'agi_request': 'agi://localhost:8080/agi/',

        'agi_channel': 'SIP/100-00000001',
        'agi_language': 'ru',
        'agi_uniqueid': '1532375920.8',
        'agi_version': '14.0.1',
        'agi_callerid': '100',
        'agi_calleridname': 'test',
        'agi_callingpres': '0',
        'agi_callingani2': '0',
        'agi_callington': '0',
        'agi_callingtns': '0',
        'agi_dnid': '101',
        'agi_rdnis': 'unknown',
        'agi_context': 'from-internal',
        'agi_extension': '101',
        'agi_priority': '1',
        'agi_enhanced': '0.0',
        'agi_accountcode': '',
        'agi_threadid': '139689736754944',
    }
    async with AGIClientSession(headers=headers, loop=loop) as session:
        async with session.sip('agi://localhost:8080/hello/?a=test1&b=var1') as response:
            async for message in response:
                agi_client_logger.debug(message)
                await response.send(AGIMessage(AGICode.OK, '0', {}))

        async with session.sip('agi://localhost:8080/hello-view/?a=test2&b=var2') as response:
            async for message in response:
                agi_client_logger.debug(message)
                await response.send(AGIMessage(AGICode.OK, '0', {}))


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '(%(levelname)s) | %(name)s | [%(asctime)s]: '
                      'File %(pathname)s:%(lineno)s" - %(funcName)s() | %(message)s'
        },
        'additional_verbose': {
            'format': '\n\033[1;34m(%(levelname)s) | %(name)s | [%(asctime)s]:\n'
                      'File \033[3;38;5;226m"%(pathname)s:%(lineno)s"\033[23;38;5;87m - %(funcName)s()\n'
                      '\033[1;34mMessage \033[3;38;5;198m"%(message)s"\n\033[23;0;0m',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'additional_verbose',
        },
    },
    'loggers': {
        'aioagi': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'py.warnings': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'asyncio': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    }
}


if __name__ == '__main__':
    logging.config.dictConfig(LOGGING)
    logging.captureWarnings(True)

    _loop = asyncio.get_event_loop()
    try:
        _loop.run_until_complete(test_request(_loop))
    except KeyboardInterrupt:
        pass

    finally:
        _loop.close()
