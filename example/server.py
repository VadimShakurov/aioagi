import logging.config

from aioagi import runner
from aioagi.app import AGIApplication
from aioagi.log import agi_server_logger
from aioagi.urldispathcer import AGIView

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '(%(levelname)s) | %(name)s | [%(asctime)s]: '
                      'File %(pathname)s:%(lineno)s" - %(funcName)s() | %(message)s'
        },
        'additional_verbose': {
            'format': '\n\033[1;34m(%(levelname)s) | %(name)s | [%(asctime)s]:\n'
                      'File \033[3;38;5;226m"%(pathname)s:%(lineno)s"\033[23;38;5;87m - %(funcName)s()\n'
                      '\033[1;34mMessage \033[3;38;5;198m"%(message)s"\n\033[23;0;0m',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'additional_verbose',
        },
    },
    'loggers': {
        'aioagi': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'py.warnings': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'asyncio': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    }
}


async def hello(request):
    message = await request.agi.stream_file('hello-world')
    await request.agi.verbose('Hello handler: {}.'.format(request.rel_url.query))
    agi_server_logger.debug(message)


class HelloView(AGIView):
    async def sip(self):
        message = await self.request.agi.stream_file('hello-world')
        await self.request.agi.verbose('HelloView handler: {}.'.format(self.request.rel_url.query))
        agi_server_logger.debug(message)


if __name__ == '__main__':
    logging.config.dictConfig(LOGGING)
    app = AGIApplication()
    app.router.add_route('SIP', '/hello/', hello)
    app.router.add_route('SIP', '/hello-view/', HelloView)
    runner.run_app(app)
