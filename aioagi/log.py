import logging

agi_access_logger = logging.getLogger('aioagi.access')
agi_client_logger = logging.getLogger('aioagi.client')
agi_internal_logger = logging.getLogger('aioagi.internal')
agi_server_logger = logging.getLogger('aioagi.server')
agi_app_logger = logging.getLogger('aioagi.app')
agi_ami_logger = logging.getLogger('aioagi.ami')
